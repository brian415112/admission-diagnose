import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

import { AdmissionDiagnoseComponent } from 'admission-diagnose/src/public-api';
import { SharedService } from './shared.service';

import { AdmissionNote } from '@his-viewmodel/electronic-medical-record/src/app';
import { AdmissionNoteSpec } from 'admission-diagnose/src/lib/admission-diagnose';
import '@his-base/date-extension';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, AdmissionDiagnoseComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app-showcase';

  #sharedService: SharedService = inject(SharedService);

  admissionNoteSpecs: AdmissionNoteSpec[] = []

  /** 執行搜索操作時被調用
   * @param rangeDates
   */
  onSearch(rangeDates: Date[]): void {

    this.#sharedService.getAdmissionNoteSpecs().subscribe((admissionNoteSpecs: AdmissionNoteSpec[]) => {
      let rangeResponse: AdmissionNoteSpec[] = admissionNoteSpecs
      rangeResponse = admissionNoteSpecs.filter((admissionNoteSpec: AdmissionNoteSpec) => {

        let tempDate: Date = new Date(admissionNoteSpec.admissionDate)
        tempDate.setHours(0, 0, 0, 0)

        return tempDate.between(rangeDates[0], rangeDates[1])
      })

      this.admissionNoteSpecs = rangeResponse
    })
  }
}
