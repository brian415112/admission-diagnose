import { Injectable, inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AdmissionNote } from "@his-viewmodel/electronic-medical-record/src/app";
import { AdmissionNoteSpec } from "admission-diagnose/src/lib/admission-diagnose";


@Injectable({
  providedIn: 'root'
})
export class SharedService {

  http: HttpClient = inject(HttpClient);

  /**
   * 獲取 admissionNoteSpecs
   * @returns Observable<admissionNoteSpecs[]>
   */
  getAdmissionNoteSpecs(): Observable<AdmissionNoteSpec[]> {
    return this.http.get<AdmissionNoteSpec[]>('assets/json/admission-diagnose.json');
  }
}
