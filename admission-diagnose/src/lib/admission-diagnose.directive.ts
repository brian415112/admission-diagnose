import { Directive, ElementRef, NgZone, AfterViewInit, Input, Inject, PLATFORM_ID, Renderer2, ViewContainerRef } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { Tooltip } from 'primeng/tooltip';

@Directive({
  selector: '[pCopyTooltip]',
  standalone: true
})
export class AdmissionDiagnoseDirective extends Tooltip implements AfterViewInit {

  @Input('pCopyTooltip') set copyNormal(copyNormal: string) {
    this._tooltipOptions.tooltipLabel = copyNormal
    this.#copyNormal = copyNormal
  }
  @Input('tooltipCopySuccess') copySuccess: string = 'success'
  @Input('tooltipCopyFail') copyFail: string = 'fail'
  @Input('tooltipCopyKey') copyKey: string = ''

  platformIdAccess: any
  #copyNormal: string = 'copy'

  constructor(@Inject(PLATFORM_ID) platformId: any, el: ElementRef<any>, zone: NgZone, config: PrimeNGConfig, renderer: Renderer2, viewContainer: ViewContainerRef) {
    super(platformId, el, zone, config, renderer, viewContainer);
    this.platformIdAccess = platformId
  }

  override onInputClick(e: Event): void {
    this.activate()

    const content: HTMLElement = document.getElementById(this.copyKey)!

    if (content) {
      navigator.clipboard.writeText(content.innerHTML).then(() => {
        this._tooltipOptions.tooltipLabel = this.copySuccess;
        this.updateText()
        this.align()

      }).catch(() => this._tooltipOptions.tooltipLabel = this.copyFail)
    }
  }

  override onMouseEnter(): void {
    this.clearTimeouts()
    this._tooltipOptions.tooltipLabel = this.#copyNormal;

    if (!this.container && !this.showTimeout) {
      this.activate();
    }
  }
}
