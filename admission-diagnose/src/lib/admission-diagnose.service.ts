import { Injectable } from '@angular/core';
import { AdmissionNoteSpec } from './admission-diagnose';

@Injectable({
  providedIn: 'root'
})
export class AdmissionDiagnoseService {

  constructor() { }

  admissionNoteSpecs: AdmissionNoteSpec[] = []

  // preprocessAdmissionNoteSpecs(admissionNoteSpecs: AdmissionNoteSpec[]): void {
  //   this.admissionNoteSpecs = admissionNoteSpecs

  //   this.admissionNoteSpecs.map((admissionNoteSpec: AdmissionNoteSpec) => {
  //     admissionNoteSpec.admissionDate = new Date(admissionNoteSpec.admissionDate)
  //     admissionNoteSpec.confirmTime = new Date(admissionNoteSpec.confirmTime)
  //     admissionNoteSpec.reportTime = new Date(admissionNoteSpec.reportTime)
  //   })
  // }
}
