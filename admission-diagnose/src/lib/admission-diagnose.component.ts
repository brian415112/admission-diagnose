import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DividerModule } from 'primeng/divider';
import { TooltipModule } from 'primeng/tooltip';
import { ButtonModule } from 'primeng/button';
import { TagModule } from 'primeng/tag';
import { PanelModule } from 'primeng/panel';

import { RowComponent, RowContentComponent, RowSidebarComponent } from '@his-directive/row-panel/dist/row-panel';
import { Descriptions } from '@his-base/datatypes/dist';
import '@his-base/datatypes'

import { AdmissionDiagnoseService } from './admission-diagnose.service';
import { AdmissionDiagnoseDirective } from './admission-diagnose.directive';
import { AdmissionNoteSpec } from './admission-diagnose';


let RowModule = [RowComponent, RowContentComponent, RowSidebarComponent];
const primeGroup = [TableModule, MultiSelectModule, DividerModule, TooltipModule, ButtonModule, TagModule, PanelModule]


@Component({
  selector: 'his-admission-diagnose',
  standalone: true,
  imports: [primeGroup, RowModule, CommonModule, FormsModule, AdmissionDiagnoseDirective],
  templateUrl: './admission-diagnose.component.html',
  styleUrls: ['./admission-diagnose.component.scss']
})
export class AdmissionDiagnoseComponent {
  #admissionDiagnoseService: AdmissionDiagnoseService = inject(AdmissionDiagnoseService)

  @Input()
  set admissionNoteSpecs(admissionNoteSpecs: AdmissionNoteSpec[]) {
    this.#admissionNoteSpecs = admissionNoteSpecs;

    // this.#admissionDiagnoseService.preprocessAdmissionNoteSpecs(this.#admissionNoteSpecs)
  }

  get admissionNoteSpecs(): AdmissionNoteSpec[] {
    return this.#admissionNoteSpecs
  }

  @Output() search: EventEmitter<Date[]> = new EventEmitter<Date[]>();

  #admissionNoteSpecs: AdmissionNoteSpec[] = []
  selectedAdmissionNoteSpec!: AdmissionNoteSpec

  /** 查詢日期事件
   * @param date
   */
  onSearchDate(rangeDates: Date[]): void {
    this.search.emit(rangeDates);
  }

  /** 獲取 Descriptions 物件中的所有屬性的 key
   *
   * @param descriptions
   * @returns
   */
  getDescriptionsKeys(descriptions: Descriptions): string[] {
    return Object.keys(descriptions);
  }

  /** 獲取日期格式化文字
   * @param date
   * @returns 日期格式化文字
   */
  getFormatDate(date: Date): string {
    let tempDate = new Date(date)

    return tempDate.formatString('YYYY-MM-DD')
  }
}
