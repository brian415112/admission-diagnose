import { Coding } from '@his-base/datatypes/dist'
import { AdmissionNote } from '@his-viewmodel/electronic-medical-record/src/app'

export class AdmissionNoteSpec extends AdmissionNote {
  admissionDate: Date = new Date()
  admissionDepartment: Coding = new Coding()
  diagnostics: Diagnosis[] = []

  /** 建構式
   ** @param that AdmissionNote
   **/
  constructor(that?: Partial<AdmissionNoteSpec>) {

    super();
    Object.assign(this, structuredClone(that));
  }
}

export class Diagnosis {
  category: string = "";
  icd: string = "";
  clinicalDescription: string = "";
  icdEng: string = "";
  issuedBy: string = "";
  issuedTime: Date = new Date();

  constructor(that?: Partial<Diagnosis>) {
    Object.assign(this, structuredClone(that));
  }
}
